__author__ = "Partha Das"


import boto3
import ConfigReader
import json
from peewee import *
import ConfigParser


Configurations = ConfigReader.read_config('Analytics_Config.cfg')
sqs = boto3.resource('sqs')
queue = sqs.get_queue_by_name(QueueName='AnalyticsQueue')


while True:
    messages = queue.receive_messages()
    if len(messages) > 0:
        for message in messages:
            analytic_ball = process_message(message.body, Configurations)
            if not analytic_ball == -1:
                _ = Analytic.create(JSON_Obj=analytic_ball[0], MicroService=analytic_ball[1], Date=analytic_ball[2])
            #message.delete() #Enable to deletes the given message from the queue
    else:
        break





config = ConfigParser.ConfigParser()
config.add_section('test1')
config.set('test1','num',2)
config.set('test1','data1','analysis')
config.set('test1','data2','test results')
config_file = open('TestConfig.cfg','a')
config.write(config_file)
config_file.close()
