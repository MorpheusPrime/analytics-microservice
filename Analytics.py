__author__ = "Partha Das"


import boto3
import ConfigReader
import json
from peewee import *


#TODO: Enable the message deletion. Just comment out the part, once the testing is satisfactory
#TODO: Add Logging. Although only thing to be logged is how many messages has been logged and from which microservice. A case of "Who watches the Watchdog" (couldn't help making the reference :D)
#TODO: Add in the docstrings and descriptions for the module.
#TODO: The Configuration location is hardcoded (don't like it). Should be changed to an argument to the main function call.
#TODO: (Optional) Add a requirements file for the microservice
#TODO: Add new module/microservice to handle microservice registration for this microservice
#TODO: Consider case when the queue is very large and the time taken to iterate through the queue takes more than the visibility time. This could end up in the messages being shown multiple times.: Solved. Just need to enable deleting the messages once they are processed, from the queue before reading the next one. 
db = SqliteDatabase('AnalyticsStore.db')

class Analytic(Model):
	JSON_Obj = TextField()
	MicroService = TextField()
	Date = TextField()

	class Meta:
		database = db


def process_message(message, Configurations):
	message = json.loads(message)
	if "Subject" in message:
		microservice = message["Subject"]
		if microservice in Configurations:
			options = Configurations[microservice]
			message_content = message["Message"].split(',')
			message_content = [x.strip() for x in message_content]
			date_time = message["Timestamp"].split("T")
			num_values = int(options["num_data"])
			tmp = "data"
			tmp_dict = {}
			for ind in range(num_values):
				name = options[tmp+str(ind + 1)]
				tmp_dict[name] = message_content[ind]
			tmp_dict["Name"] = options["name"]
			tmp_dict = json.dumps(tmp_dict)
			return_object = [tmp_dict, microservice, date_time[0]]
			#print (return_object)
			return return_object
	else:
		print ("Error! No subject specified in message! Check following Message for errors")
		print (message)
	return -1

Configurations = ConfigReader.read_config('Analytics_Config.cfg')
sqs = boto3.resource('sqs')
queue = sqs.get_queue_by_name(QueueName='AnalyticsQueue')


db.connect()
try:
	Analytic.create_table()
except:
	print ("Database already exist, loading that instead")

while True:
	messages = queue.receive_messages()
	if len(messages) > 0:
		for message in messages:
			analytic_ball = process_message(message.body, Configurations)
			if not analytic_ball == -1:
				_ = Analytic.create(JSON_Obj=analytic_ball[0], MicroService=analytic_ball[1], Date=analytic_ball[2])
			#message.delete() #Enable to deletes the given message from the queue
	else:
		break

#Dev Journal:
#Setup everything. Everything seems to work (I know I'll regret saying that). Only one quirk. Boto doesn't seem to be sending the messages every time I query it. It sometimes returns just blank.
#Intuuition says could be the invisible limit, but I tried it after more than 30 seconds (my visibility timeout), still doesn't seem to work. Current fix is just to re-run it. Could be an AWS thing. So if nothing comes up, just rerun.
#Like always, wrapping this thing up in a class could be a good idea. Need to discuss with Swen.
#Discussion with Lex: Think there is a new need of a microservice within the microservice to handle the registration of the microservices, rather registering them manually. (So meta! @_@)
#One way to solve the large queues and seeing the same messages multiple times is to increase the visibility timeout of the queue, something like an hour or so (if it supports it). Then again, the messages will be deleted as soon as processed. So, shouldn't be a problem after all.
