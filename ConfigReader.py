import configparser


def read_config(config_file_path):
	config = configparser.ConfigParser()
	config.read(config_file_path)
	sections = config.sections()
	configuration = {}
	for section in sections:
		sub_section_options = config.options(section)
		options = {}
		for option in sub_section_options:
			options [option] = config.get(section, option)
		configuration [section] = options
	return configuration

#Configs = read_config("Analytics_Config.cfg")
#for key,val in Configs.items():
	#print ("Key: %s\tVal: %s"%(key, val))

