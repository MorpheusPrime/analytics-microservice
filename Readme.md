Analytics Engine:

Section I.

This microservice aggregates various diagnostic messages and aggregates them in a common database to be accessed and analysed (perhaps extended with various visualizations), to give an insight about the proper functioning (or lack thereof) of the entire system, across microservices.

The microservice acts like an engine. It employs a registration and subscription model. To be tracked by this microservice the following steps are needed to be performed:

	1) Register with the Analytics Microservice (details below)
	2) Subscribe to the analytics_microservice

Once these are done, the Microservice will track the registered Microservice and parse their diagnostic messages and store them as a JSON object in a database, where the records can be retrieved by the name of the microservice that sent it along with the date it was sent on.

Section II.

How to Register:

The Analytics microservice uses a configuration file in order to be flexible enough for newer microservices to be added easily without the need for the code to be modified. Once a microservice is registered through the Configuration file, it will betracked. The configuration file (Analytics_Configuration.cfg) is in the following format:

[<name of the microservice>] (1)
name: <Display name of the microservice> (2)
num_data: <Number of diagnostic fields> (3)
data1: <Display name of the first diagnostic field>
data2: <Display name of the second diagnostic field>
data3: <Display name of the third diagnostic field>

Here, (1) indicates the name if the microservice that will be sending the message. This should be an exact match to the microservice name. The name will be read from the message queue and so, if it doesn't match, the message will be ignored.
(2) is just the Display name of the microservice to be displayed when the diagnostic is called. It is just there to increase human readability later when the reports are being analysed.
(3) is the number of diagnostic messages that the microservice sends. So if a microservice sends like 2 messages about the # number of pages processed and # number of pages stored, then this will be 2.
Following the previous one, will be the field name starting from 1 to the number of data specified above. The format of the option has to be the same as "data<number>" as this pattern is matched to retrieve the corresponding value from the message. So the ORDER IS IMPORTANT. The names stored against these options are, again, used to increase human and report readability.

An example of the configuration would be as follows:

[analytics_microservice]
name: Analytics
num_data: 3
data1: Pages Diffed
data2: New Lines
data3: Errors

Here the registered microservice is the Analytics Microservice. The Display name in the report will Analytics. It sends 3 attribute values as its diagnostic messgaes.Namely, they are The number of pages Diffed, the number of newlines added and the number of errors occured.

Section III.

To send messages, the microservices just needs to publish to the analytics_microservice topic, with the name of the microservice in the Subject field and the value for the fields specified in the registration, separated by commas. Again, the order is important. An example for the previous registration would be as follows:

Suppose the analytics_microservice got 357 new lines from 500 diffed pages with 58 errors. So the message that will be sent to the Analytics topic would be in the format:

Subject: analytics_microservice
Message: 500,357,58

Notice that the order of the information presented was different than the registered details. So, the message body had to be switched to reflect that order.

Only the value is sent as a message as opposed to the name: value pair, because in this way we can save up a lot on the size of the messages, while avoiding redundant information and operations.
