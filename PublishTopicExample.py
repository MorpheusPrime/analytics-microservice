import boto3


#REQUIRED: The TopicArn. Can be found from the console
client = boto3.client('sns')
response = client.publish(TopicArn='arn:aws:sns:eu-west-1:596091452785:analytics_microservice', Message='Testing,2,world', Subject='Microservice')
#The response contains a dictionary with the messageid, requestid and HTTP Status code. To see if the message went through use the following code
status = response['ResponseMetadata']['HTTPStatusCode']
#This should be equal to 200 if the message was sent successfully, so should be a good thing to check.
