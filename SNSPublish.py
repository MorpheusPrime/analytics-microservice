__author__ = "Partha Das"

"""Function to publish to a SNS topic given the TopicArn and the message. Both Subject and Body is REQUIRED.
Returns the status code of the message"""

import boto3


#REQUIRED: The TopicArn. Can be found from the console

def publishToTopic(TopicArn, Subject, Message):
	client = boto3.client('sns')
	response = client.publish(TopicArn=TopicArn Message=Message, Subject=Subject)
	#The response contains a dictionary with the messageid, requestid and HTTP Status code. To see if the message went through use the following code
	status = response['ResponseMetadata']['HTTPStatusCode']
	#This should be equal to 200 if the message was sent successfully, so should be a good thing to check.
	return status
